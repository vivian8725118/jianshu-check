import threading

from sanic import Sanic

from view import ReviewList

from view.ReviewList import background_save, background_save_shutdown

app = Sanic(__name__)

app.blueprint(ReviewList.review_list_bp)

t = threading.Thread(target=background_save, daemon=True)
t.start()


@app.listener('before_server_stop')
async def notify_server_stopping(app, loop):
    background_save_shutdown()


app.run(host='0.0.0.0', port=8000, debug=False)
