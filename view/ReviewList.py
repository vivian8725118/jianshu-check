import time
from sanic import Blueprint
from jinja2 import Environment, PackageLoader
from sanic.response import html, text, json

from file_db import FileDB

env = Environment(loader=PackageLoader('__main__', 'templates'))
# env = Environment(loader=jinja2.FileSystemLoader(os.path.join(os.getcwd(), '/templates')))
review_list_bp = Blueprint('js')

white_name_list, black_name_list, keyword_list, recommend_list = FileDB.load_list()


@review_list_bp.route('/view/white_list', methods=['GET'])
async def get_white_list(request):
    template = env.get_template('index.html')
    content = template.render(list=white_name_list)
    return html(content)


@review_list_bp.route('/view/black_list', methods=['GET'])
async def get_white_list(request):
    template = env.get_template('index.html')
    content = template.render(list=black_name_list)
    return html(content)


@review_list_bp.route('/view/keyword_list', methods=['GET'])
async def get_white_list(request):
    template = env.get_template('index.html')
    content = template.render(list=keyword_list)
    return html(content)


@review_list_bp.route('/view/recommend_list', methods=['GET'])
async def get_white_list(request):
    template = env.get_template('index.html')
    content = template.render(list=recommend_list)
    return html(content)


@review_list_bp.route('/add/white_list/<name>', methods=['GET'])
async def get_white_list(request, name):
    if name in white_name_list:
        return text('already exists')
    white_name_list.append(name)
    return text('add success!')


@review_list_bp.route('/add/black_list/<name>', methods=['GET'])
async def get_white_list(request, name):
    if name in black_name_list:
        return text('already exists')
    black_name_list.append(name)
    return text('add success!')


@review_list_bp.route('/add/keyword_list/<name>', methods=['GET'])
async def get_white_list(request, name):
    if name in keyword_list:
        return text('already exists')
    keyword_list.append(name)
    return text('add success!')


@review_list_bp.route('/add/recommend_list/<name>', methods=['GET'])
async def get_white_list(request, name):
    if name in recommend_list:
        return text('already exists')
    recommend_list.append(name)
    return text('add success!')


@review_list_bp.route('/del/white_list/<name>', methods=['GET'])
async def get_white_list(request, name):
    if name in white_name_list:
        white_name_list.remove(name)
        return text('del success!')
    return text('{} not in list'.format(name))


@review_list_bp.route('/del/black_list/<name>', methods=['GET'])
async def get_white_list(request, name):
    if name in black_name_list:
        black_name_list.remove(name)
        return text('del success!')
    return text('{} not in list'.format(name))


@review_list_bp.route('/del/keyword_list/<name>', methods=['GET'])
async def get_white_list(request, name):
    if name in keyword_list:
        keyword_list.remove(name)
        return text('del success!')
    return text('{} not in list'.format(name))


@review_list_bp.route('/del/recommend_list/<name>', methods=['GET'])
async def get_white_list(request, name):
    if name in recommend_list:
        recommend_list.remove(name)
        return text('del success!')
    return text('{} not in list'.format(name))


@review_list_bp.route('/get/white_list', methods=['GET'])
async def get_white_list(request):
    return json(white_name_list)


@review_list_bp.route('/get/black_list', methods=['GET'])
async def get_white_list(request):
    return json(black_name_list)


@review_list_bp.route('/get/keyword_list', methods=['GET'])
async def get_white_list(request):
    return json(keyword_list)


@review_list_bp.route('/get/recommend_list', methods=['GET'])
async def get_white_list(request):
    return json(recommend_list)


def background_save():
    while True:
        print('timer start write.')
        FileDB.write_list(white_name_list, black_name_list, keyword_list, recommend_list)
        print('timer end write.')
        time.sleep(60 * 30)


def background_save_shutdown():
    print('shutdown start write.')
    FileDB.write_list(white_name_list, black_name_list, keyword_list, recommend_list)
    print('shutdown end write.')
